import { Button } from '@/components/ui/button'
import { Input } from '@/components/ui/input'
import { Metadata } from 'next'
import React, {FC} from 'react'
import { handleSignIn } from './action'

interface SignInPageProps {

}

export const metadata: Metadata ={
    title: "Dashboard | Sign-In",
    description: "",
}

const SignInPage: FC<SignInPageProps> = ({}) => {
  return (
    <div className="w-full h-screen">
        <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
            <div className="sm:mx-auto sm:w-full sm:max-w-sm">
                <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-800">
                    Sign in to your account
                </h2>
            </div>
            <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
                <form action={handleSignIn} className="mb-4 space-y-6">
                    <Input className="rounded-md shadow-sm" type='email' placeholder='Email address' name='email' required/>
                    <Input className="rounded-md shadow-sm" type='password' placeholder='Password' name='password' required/>
                    <Button className="w-full" type='submit'>Sign in</Button>
                </form>
            </div>
        </div>
    </div>
  )
}

export default SignInPage