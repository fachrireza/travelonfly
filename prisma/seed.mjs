
import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcrypt';
// import { PrismaClientKnownRequestError } from '@prisma/client/runtime/library';

const prisma = new PrismaClient();

async function main() {
    const saltRounds = 10;
    const hashedPassword = bcrypt.hashSync("admin123", saltRounds);
    try {
      const userSeed = await prisma.user.create({
          data: {
              name: "Admin",
              email: "admin@gmail.com",
              role: "ADMIN",
              password: hashedPassword
          }
      })
      console.log("User created:", userSeed);
    } catch (error) {
        if (error instanceof PrismaClientKnownRequestError && error.code === 'P2002') {
            console.error("Error creating user:", error.meta);
        } else {
            throw error;
        }
    }
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })